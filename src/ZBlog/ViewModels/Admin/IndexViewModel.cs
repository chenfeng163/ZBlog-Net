﻿namespace ZBlog.ViewModels.Admin
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        
        public bool BrowserRemembered { get; set; }
    }
}
